package com.e.pruebafirebase;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.e.pruebafirebase.Model.Experiencia;
import com.e.pruebafirebase.Model.Persona;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class RegistroExperiencias extends AppCompatActivity {
    TextView t1;
    Button b1;
    EditText ocupacion, anos, descripcion;
    private FirebaseFirestore db;
    CollectionReference per;
    List<Persona> idemple = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro_experiencia);
        t1 = findViewById(R.id.NombreEmpleado);
        ocupacion = findViewById(R.id.OCU);
        anos= findViewById(R.id.anosExperiencia);
        descripcion = findViewById(R.id.Descripcion);
        db = FirebaseFirestore.getInstance();
        b1 = findViewById(R.id.agregarExperiencia2);
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            String value2 =extras.getString("Correo");
            t1.setText(value2);
            consultaridempleado(value2);




        }
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                agregar();
            }
        });
    }

    public void agregar() {
        String an = anos.getText().toString();
        String des = descripcion.getText().toString();
        String ocu = ocupacion.getText().toString();
        if (an.equals("") || des.equals("") || ocu.equals("") ) {
            validation();

        } else {
            vaciar();
            Experiencia exp = new Experiencia();
            exp.setId(UUID.randomUUID().toString());
            exp.setIdempleado(idempleado());
            exp.setNombre(Nombre());
            exp.setCorreo(Correo());
            exp.setOcupacion(ocu);
            exp.setAños(an);
            exp.setDescripcion(des);
            List<Experiencia> pe = new ArrayList<>();
            pe.add(exp);
            for (Experiencia experiencia : pe) {
                Map<String, Object> mapa = new HashMap<>();
                mapa.put("id", experiencia.getId());
                mapa.put("idempleado", experiencia.getIdempleado());
                mapa.put("nombre", experiencia.getNombre());
                mapa.put("correo", experiencia.getCorreo());
                mapa.put("ocupacion", experiencia.getOcupacion());
                mapa.put("anos", experiencia.getAños());
                mapa.put("descripcion", experiencia.getDescripcion());
                db.collection("Experiencias")
                        .add(mapa)
                        .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                            @Override
                            public void onSuccess(DocumentReference documentReference) {
                                Log.d("FB", "Postulante agregado correctamente:" + documentReference.getId());
                                //Intent b1 = new Intent(MainActivity.this,LoginActivity.class);
                                //startActivity(b1);
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.w("FB", "Error Postulante no agregado", e);
                            }
                        });

            }
        }
    }

    //Consultar id del empleado
    public void consultaridempleado(String correo){
        per =db.collection("Empleado");
        Query query = per.whereEqualTo("Correo", correo);
        query.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Persona per = new Persona();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();
                                String valor= (String) md.get("id");
                                String valor2 = (String) md.get("Nombre");
                                String valor3 = (String) md.get("Correo");
                                per.setId(valor);
                                per.setNombre(valor2);
                                per.setCorreo(valor3);
                            }
                            agregaridEmpleado(per);

                        }
                    }
                });

    }

    public void agregaridEmpleado(Persona pe){
        idemple.add(pe);
    }

    public String idempleado(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< idemple.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return idemple.get(indice).getId();

        }else {
            return null;
        }
    }

    public String Nombre(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< idemple.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return idemple.get(indice).getNombre();

        }else {
            return null;
        }
    }

    public String Correo(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< idemple.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return idemple.get(indice).getCorreo();

        }else {
            return null;
        }
    }

    public void validation(){
        String an = anos.getText().toString();
        String des = descripcion.getText().toString();
        String ocu = ocupacion.getText().toString();
        if(an.equals("")){
            anos.setError("Requiered");
        }
        if(des.equals("")){
            descripcion.setError("Requiered");
        }
        if(ocu.equals("")){
            ocupacion.setError("Requiered");
        }

    }


    public void vaciar(){
        t1.setText("");
        anos.setText("");
        descripcion.setText("");
        ocupacion.setText("");

    }
}
