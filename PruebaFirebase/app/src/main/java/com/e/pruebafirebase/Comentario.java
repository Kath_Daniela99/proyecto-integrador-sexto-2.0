package com.e.pruebafirebase;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.e.pruebafirebase.Model.Comentarios;
import com.e.pruebafirebase.Model.Persona;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Comentario extends AppCompatActivity {
    private FirebaseFirestore db;
    EditText e1 ,e2;
    Button b1, b2;
    CollectionReference per;
    List<Persona> idemple = new ArrayList<>();
    List<Persona> idemplea = new ArrayList<>();
    List<Persona> nombres = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentario);
        e1 = findViewById(R.id.comentariousuario);
        e2 = findViewById(R.id.setarComentario);
        b1 = findViewById(R.id.Agregarcomentario2);
        b2 = findViewById(R.id.consul);
        db = FirebaseFirestore.getInstance();
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            String value2 =extras.getString("Correo");
            consultaridempleado(value2);

        }


        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String correo = e1.getText().toString();
                consultaridempleador(correo);
                consultarnombres(correo);

            }
        });

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                agregar();
                Intent b3= new Intent(Comentario.this, ListaComentario.class);
                startActivity(b3);
            }
        });
    }



    public void agregar(){
        String com = e2.getText().toString();
        Comentarios p = new Comentarios();
        p.setIdcomentario(UUID.randomUUID().toString());
        p.setIdempleado(idempleado());
        p.setComentario(com);
        p.setNombre(idnombre());
        p.setIdempleador(idempleador());
        ArrayList<Comentarios> pe = new ArrayList<>();
        pe.add(p);
        for (Comentarios comentarios : pe) {
            Map<String,Object> mapa = new HashMap<>();
            mapa.put("idcomentario",comentarios.getIdcomentario());
            mapa.put("idempleado",comentarios.getIdempleado());
            mapa.put("comentario",comentarios.getComentario());
            mapa.put("nombre",comentarios.getNombre());
            mapa.put("idempleador",comentarios.getIdempleador());
            db.collection("Comentarios")
                    .add(mapa)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            Log.d("FB","Comentario agregado correctamente:" + documentReference.getId());
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.w("FB","Error Comentario no agregado",e);
                        }
                    });

        }
    }
    //Consultar id del empleado
    public void consultaridempleado(String correo){
            per =db.collection("Empleado");
            Query query = per.whereEqualTo("Correo", correo);
            query.get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                Persona per = new Persona();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Map<String,Object> md = document.getData();
                                    String valor= (String) md.get("id");
                                    per.setId(valor);
                                }
                                agregaridEmpleado(per);

                            }
                        }
                    });

        }

        public void agregaridEmpleado(Persona pe){
            idemple.add(pe);
        }

        public String idempleado(){
            boolean valor=false;
            int indice=0;
            for (int i=0;i< idemple.size();i++){
                valor=true;
                indice=i;
            }
            if(valor){
                return idemple.get(indice).getId();

            }else {
                return null;
            }
    }
    // Consultar id del empleador
    public void consultaridempleador(String correo){
        per =db.collection("Empleador");
        Query query = per.whereEqualTo("Correo", correo);
        query.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Persona per = new Persona();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();
                                String valor= (String) md.get("id");
                                per.setId(valor);
                            }
                            agregaridEmpleador(per);

                        }
                    }
                });

    }

    public void agregaridEmpleador(Persona pe){

        idemplea.add(pe);
    }

    public String idempleador(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< idemplea.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return idemplea.get(indice).getId();

        }else {
            return null;
        }
    }

    // Consultar Nombre
    public void consultarnombres(String correo){
        per =db.collection("Empleador");
        Query query = per.whereEqualTo("Correo", correo);
        query.get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            Persona per = new Persona();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Map<String,Object> md = document.getData();
                                String valor= (String) md.get("Nombre");
                                per.setNombre(valor);
                            }
                            nom(per);

                        }
                    }
                });

    }

    public void nom(Persona pe){
        nombres.add(pe);
    }

    public String idnombre(){
        boolean valor=false;
        int indice=0;
        for (int i=0;i< nombres.size();i++){
            valor=true;
            indice=i;
        }
        if(valor){
            return nombres.get(indice).getNombre();

        }else {
            return null;
        }
    }


}
